﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Coesolutions.Modules.NobraVTCSchedule.View" %>

<%--<link href="/DesktopModules/NobraVTCSchedule/module.css" rel="stylesheet" />
<script src=  "/DesktopModules/NobraVTCSchedule/js/kendo.core.min.js"></script>
<link href="/DesktopModules/NobraVTCSchedule/styles/kendo.common.min.css" rel="stylesheet" />
<link href="/DesktopModules/NobraVTCSchedule/styles/kendo.default.min.css" rel="stylesheet" />
<script src="/DesktopModules/NobraVTCSchedule/js/kendo.all.min.js"></script>
<script src="/DesktopModules/NobraVTCSchedule/content/shared/js/jszip.js"></script>
<script src="/DesktopModules/NobraVTCSchedule/content/shared/js/pako.min.js"></script>
<script src="/DesktopModules/NobraVTCSchedule/js/moment.js"></script>
--%>

<link href="/nobra/DesktopModules/NobraVTCSchedule/module.css" rel="stylesheet" />
<script src="/nobra/DesktopModules/NobraVTCSchedule/js/kendo.core.min.js"></script>
<link href="/nobra/DesktopModules/NobraVTCSchedule/styles/kendo.common.min.css"" rel="stylesheet" />
<link href="/nobra/DesktopModules/NobraVTCSchedule/styles/kendo.default.min.css" rel="stylesheet" />
<script src="/nobra/DesktopModules/NobraVTCSchedule/js/kendo.all.min.js"></script>
<script src="/nobra/DesktopModules/NobraVTCSchedule/content/shared/js/jszip.js"></script>
<script src="/nobra/DesktopModules/NobraVTCSchedule/content/shared/js/pako.min.js"></script>
<script src="/nobra/DesktopModules/NobraVTCSchedule/js/moment.js"></script>



<script>

    $(document).ready(function () {
        //console.log("ready!");
        loadComboBoxes();

        $("#btnFetchVTC").kendoButton({
            click: function (e) {
                //alert(e.event.target.tagName);
                
                if ($("#groupselectionVTC").val().length > 0 && $("#yearselectionVTC").val().length > 0) {
                    getDataVTC($("#groupselectionVTC").val(),
                        $("#yearselectionVTC").val());
                }
                else {
                    alert('check your selections');
                }

                


                
            }
        });

        
    });


   
    function loadComboBoxes() {
        $("#groupselectionVTC").kendoComboBox({
            dataSource: {
                data: [
                    "A"
                    , "B"
                ]
            }
        });



        $("#yearselectionVTC").kendoComboBox({
            dataSource: {
                data: [
                    "2016"
                    , "2017"
                     , "2018"
                      , "2019"
                       , "2020"
                        , "2021"

                ]
            }
        });
    }




    function getDataVTC(g,y) {


        var params = {
            group: g,
            year: y
        };
      

        var me = location.host;
        var myUrl = "/DesktopModules/NOBRADataService/API/Main/GetVTCByGroupYear"

        if (me.indexOf('nobra') >= 0) {
            myUrl = '/nobra' + myUrl;
        }


        $.ajax({
            url: myUrl,
            type: 'GET',
            data: params,
            dataType: 'json',
            success: function (data) {
                //WriteResponse(data);
               // alert('Happy, Happy');
                parseDataVTC(data);
                


            },
            error: function () {
                alert('Error on data call');
            }
        });

    }


    function parseDataVTC(obj) {

        //for (var Group in obj) {
        //    if (obj.hasOwnProperty(Group)) {
        //        alert(Group + " -> " + obj[Group]);
        //    }
        //}

        var col = obj.VTClists;


        //var col0 = col[0].WatchGroup;
        //alert('Watchgroup is ' + col0);
        for (var i = 0; i < col.length;i++)
        {
           // console.log('Watchgroup is ' + col[i].WatchGroup);
            bindGridVTC(i, col[i]);
        }



    }

    function bindGridVTC(gid, vtcl) {

        var strTitle = $("#groupselectionVTC").val() + "-Watch " + $("#yearselectionVTC").val() + " VTC";
        $('#divTitle').html(strTitle);

        //$("#ha" + gid).html(vtcl.WatchGroup);
       // $("#hb" + gid).html(vtcl.StartDate);

        var fldTitle = vtcl.WatchGroup;
        var fldValue = vtcl.StartDate;              //vtcl.StartDate;          //String(moment(vtcl.StartDate).format("MM/DD/YYYY")) //vtcl.StartDate;
       // $("#hb" + gid).html

    //    var headerJSON = {
    //        fldTitle: fldValue
        //};



        var headerRecordsVTC = new kendo.data.DataSource({


           
            //schema: {
            //    model: {
            //        fields: {
                        
            //            fldTitle: { type: "string" }

            //        }
            //    }
            //},

            data: [
                {
                    fldTitle: fldTitle
                },
                 {
                     fldTitle: String(fldValue)
                 }


                ]

                //headerJSON
        });

        $("#ha" + gid).kendoGrid({
            //columns: [
            //               {
            //        field: fldTitle,
            //        //format: "{0: MM/DD/YYYY}"
                  
            //  }
            //],
            dataSource: headerRecordsVTC,

            scrollable: false

        }).addClass("hdr");;
        


        var vtcrecords = new kendo.data.DataSource({
            

         
            schema: {
                model: {
                    fields: {
                        PilotName: { type: "string" },
                        VtcTime: { type: "date" }

                    }
                }
            },

            data:vtcl.VTCRecords
        });

        $("#gridVTC" + gid).kendoGrid({
            columns: [
              {
                  field: "PilotName"
                  , width: "150px"
              },
              {
                  field: "VtcTime",
                  format: "{0: HHmm}"
                  , width: "50px"
              }
            ],
            dataSource: vtcrecords,

            scrollable: false
            
        });

        $('#retrieveTime').html(moment().format('MM/DD/YYYY HH:mm:ss'));
    }

    function getFileNameString() {

        var vtcFileName = "VTC_Group_" + String($("#groupselectionVTC").val()) + "_" + String($("#yearselectionVTC").val()) + ".pdf";

        return vtcFileName
    }

    </script>


<script>
   
</script>

<input id="groupselectionVTC" placeholder="Select Group..." style="width: 20%;" />
<input id="yearselectionVTC" placeholder="Select Year..." style="width: 20%;" />
<button id="btnFetchVTC" type="button">Retrieve</button>
 <button id="btnVtcPDF" type="button">Export to PDF</button>


<div id="printVTC">
    <br />
<table class="vtable">
    <tr>
        <th colspan="4">    <div id="divTitle"  style="text-align:center;border:none";font-size: 36px;>Select and Retrieve For Display</div>
</th>
    </tr>
     <tr>
        <th colspan="4">    <div id="divBase"  style="text-align:center;border:none";font-size: 36px;>Base Schedule</div>
</th>
    </tr>
    <tr>
        <td>
             <div id="ha0"></div>
            <div id="gridVTC0"></div>
        </td>
        <td>
             <div id="ha1"></div>
            <div id="gridVTC1"></div>
        </td>
        <td>
             <div id="ha2"></div>
            <div id="gridVTC2"></div>
        </td>
        <td>
             <div id="ha3"></div>
            <div id="gridVTC3"></div>
        </td>

    </tr>
<tr>
        <td>
             <div id="ha4"></div>
            <div id="gridVTC4"></div>
        </td>
        <td>
             <div id="ha5"></div>
            <div id="gridVTC5"></div>
        </td>
        <td>
             <div id="ha6"></div>
            <div id="gridVTC6"></div>
        </td>
        <td>
             <div id="ha7"></div>
            <div id="gridVTC7"></div>
        </td>

    </tr>
<tr>
        <td>
             <div id="ha8"></div>
            <div id="gridVTC8"></div>
        </td>
        <td>
             <div id="ha9"></div>
            <div id="gridVTC9"></div>
        </td>
        <td>
             <div id="ha10"></div>
            <div id="gridVTC10"></div>
        </td>
        <td>
             <div id="ha11"></div>
            <div id="gridVTC11"></div>
        </td>

    </tr>
<tr>
        <td>
             <div id="ha12"></div>
            <div id="gridVTC12"></div>
        </td>
        <td>
             <div id="ha13"></div>
            <div id="gridVTC13"></div>
        </td>
        <td>
             <div id="ha14"></div>
            <div id="gridVTC14"></div>
        </td>
        <td>
             <div id="ha15"></div>
            <div id="gridVTC15"></div>
        </td>

    </tr>
<tr>
        <td>
             <div id="ha16"></div>
            <div id="gridVTC16"></div>
        </td>
        <td>
             <div id="ha17"></div>
            <div id="gridVTC17"></div>
        </td>
        <td>
             <div id="ha18"></div>
            <div id="gridVTC18"></div>
        </td>
        <td>
             <div id="ha19"></div>
            <div id="gridVTC19"></div>
        </td>

    </tr>
<tr>
        <td>
             <div id="ha20"></div>
            <div id="gridVTC20"></div>
        </td>
        <td>
             <div id="ha21"></div>
            <div id="gridVTC21"></div>
        </td>
        <td>
             <div id="ha22"></div>
            <div id="gridVTC22"></div>
        </td>
        <td>
             <div id="ha23"></div>
            <div id="gridVTC23"></div>
        </td>

    </tr>
<tr>
        <td>
             <div id="ha24"></div>
            <div id="gridVTC24"></div>
        </td>
        <td>
             <div id="ha25"></div>
            <div id="gridVTC25"></div>
        </td>
        <td>
            <div ></div>
        </td>
        <td>
            <div ></div>
        </td>

    </tr>

</table>
    <div id="retrieveTime"></div>

</div>

<br />


<script>
    var me = location.host;
    var myPostURL = "/DesktopModules/PugetSoundDataService/API/Main/Post";

    if (me.indexOf('nobra') >= 0) {
        myPostURL = '/nobra' + myPostURL;
    }

    //$("#groupselectionVTC").val().length > 0 && $("#yearselectionVTC").val()





    $("#btnVtcPDF").kendoButton();
    var button = $("#btnVtcPDF").data("kendoButton");
    button.bind("click", function (e) {
        kendo.drawing.drawDOM($('#printVTC'))
            .then(function (group) {
                var PAGE_RECT = new kendo.geometry.Rect(
           [0, 0], [i2p(8 - .5), i2p(10.5 - .5)]
         );
                var content = new kendo.drawing.Group();
                content.append(group);

                kendo.drawing.fit(content, PAGE_RECT)
                //kendo.drawing.pdf.saveAs(group, 'JobReport.pdf');
                // Render the result as a PDF file
                return kendo.drawing.exportPDF(content, {
                    paperSize: "letter",
                    margin: { left: "1cm", top: "1cm", right: "1cm", bottom: "1cm" }
                });
            })
            .done(function (data) {
                // Save the PDF file
                kendo.saveAs({
                    dataURI: data,
                    fileName: getFileNameString(),
                    proxyURL: myPostURL ,
                });
            });
    });
</script>

  <script>
      function i2p(val) {
          return val * 72;
      }


    </script>